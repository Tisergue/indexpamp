<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
		
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<script type="text/javascript"></script>

	<style media="screen">

		body
			{background-color:#E6E6E6;text-align:center;}
		#content
			{width:600px;height:600px;border:2px solid black;border-radius:12px;position:absolute;
			 left:50%;top:50%;margin-left:-300px;margin-top:-300px;background-color:white;overflow:hidden;}
		#table
			{overflow:scroll; max-height:520px;}
		h1
			{background-color:blue;color:white;margin-top:0;padding:5px;}
		table
			{border-collapse:collapse;width:95%;margin:auto;}
		tr, th, td
			{border:1px solid black;}
		th
			{background-color:#F6D8CE;}
		#projects
			{width:25%;}
		a
			{text-decoration: none;}

	</style>

	<title> HOME PAGE : PAMP </title>
</head>
<body>

<div id='content'>
	<h1> HOME PAGE : PAMP </h1>
	<div id='table'>
	<table>
		<tr>
			<th id='projects'> FILES </th>
			<th> LINKS </th>
		</tr>
		<?php
			$local = 'http://localhost:8080/';
			if ($dir = scandir('./'))
				foreach($dir as $key => $val)
				{
					if (is_dir($val))
					{
						if ($val !== '.' && $val !== '..')
							echo '	<tr> 
									<td> '.$val.' </td>
									<td> <a href="./'.$val.'" onclick="window.open(this.href); return false;"> '.$_SERVER['HTTP_REFERER'].$val.' </a> </td>
									</tr><tr>
									<td> </td>
									<td> <a href="'.$local.$val.'/" onclick="window.open(this.href); return false;"> '.$local.$val.'/ </a> </td> 
									</tr>';
					}
				}
		?>
	</table>
	</div>
</div>

</body>
</html>